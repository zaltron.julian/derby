#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license. 
# aguante kropotkin
import os
import random
from telegram.ext import Updater
from telegram import Update
from telegram.ext import CallbackContext, MessageHandler, Filters
from config import TOKEN
import logging

#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~#~
PORT = int(os.environ.get('PORT', '8443'))
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger('root')

lenguajeDeDerby = \
    {
    (" derby"): ["aww <3"], \
    (":c", ":("): ["te mando un abrazo :("], \
    (" open source"): ["no querrás decir software libre?"], \
    (" bug "): ["patches welcome"], \
    (" gatito", " gatita", " gatite"): ["LXS GATITXS SON LO MEJOR"], \
    (" o/", " o//", " \\o"): ["o/"], \
    (" bot ", " bot.", " bot,"): ["a quien le habla?", "que ondi, hay un bot por acá? :O", "no querras decir cyborg?"], \
    (" windows", " güindous"): ["eso en GNU/Linux no pasa xD"], \
    (" ruby"): ["ruby love <3"], \
    (" torrent"): ["compartir es bueno", "copiar no es robar", "torrent o patria", "si no torrenteamos, la cultura se netflixea"],   \
    ("bjork", "björk"): ["https://youtu.be/WXjJTdcIkkk"]
    }

saludos = ["Bienvenide,", "Holiiiiis"];
clicmojis = ["🎹", "🎼", "🎧", "🦚", "🐚", "🍄", "🥁", "🎺", "🎷", "🪗", "🎸", "🪕", "🎻", "🎲", "🎮", "💻", "🎛", "🖼", "🧮", "🩰", "📚", "👾", "🤖", "🕹", "💾", "🔊"];

def darLaBienvenida(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat_id
        
    for member in update.message.new_chat_members:
        update.message.reply_text(random.choice(saludos) + f" #{member.first_name} o/. " + random.choice(clicmojis) + " " \
                "Te invitamos a leer el código de convivencia: " \
                "https://colectivo-de-livecoders.gitlab.io/#coc")


def responderSiTieneEstasPalabras(texto:str, update: Update, context: CallbackContext):
    for palabrasParaEscuchar,respuestas in lenguajeDeDerby.items():
        if type(palabrasParaEscuchar) is str:
            respuesta = random.choice(respuestas)

            if palabrasParaEscuchar in texto:
                context.bot.send_message(chat_id=update.effective_chat.id, text=respuesta)
                break

        elif type(palabrasParaEscuchar) is tuple:
            respuesta = random.choice(respuestas)
            for palabra in palabrasParaEscuchar:
                if palabra in texto:
                    context.bot.send_message(chat_id=update.effective_chat.id, text=respuesta)
                    break


def analizarTexto(update: Update, context: CallbackContext):
    texto = update.message.text
    if type(texto) is str:
        texto = texto.lower()
    responderSiTieneEstasPalabras(texto, update, context)


def main():
    updater = Updater(token=TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(MessageHandler(Filters.status_update.new_chat_members, darLaBienvenida))
    dp.add_handler(MessageHandler(Filters.text & (~Filters.command), analizarTexto))

    updater.start_webhook(listen="0.0.0.0",
                          port=PORT,
                          url_path=TOKEN,
                          webhook_url = 'https://derbyclic.herokuapp.com/' + TOKEN)

    updater.start_polling(allowed_updates=Update.ALL_TYPES)

if __name__ == '__main__':
    main()
